package org.academiadecodigo.bootcamp;

import java.util.Random;

public enum Hand {
    ROCK("Rock"),
    PAPER("Paper"),
    SCISSORS("Scissors");

    public String choice;

    Hand(String choice){
        this.choice = choice;
    }

        public static Hand randomHand() {
            Hand[] values = Hand.values();
            int length = values.length;
            int randomIndex = new Random().nextInt(length);
            return values[randomIndex];

            //return Hand.values()[(int) (Math.random() * Hand.values().length)];
        }



}
