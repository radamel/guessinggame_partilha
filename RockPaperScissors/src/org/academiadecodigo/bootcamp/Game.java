package org.academiadecodigo.bootcamp;

import javax.sound.midi.Soundbank;

public class Game {

    public String choice;
    public int rounds;

    public int game;

    public static void startGame() {

        int rounds = 0; //Var para incrementar os rounds
        int player1Wins = 0; //Var para incrementar as wins do P1
        int player2Wins = 0; //Var para incrementar as wins do P2
        String player1Choice = Hand.randomHand().choice;
        String player2Choice = Hand.randomHand().choice;

        while (rounds < 3) {

            player1Choice = Hand.randomHand().choice;
            player2Choice = Hand.randomHand().choice;


            if (player1Choice.equals(Hand.ROCK.choice)) {

                System.out.println("Player one played " + player1Choice);
                System.out.println("Player two played " + player2Choice);

                if (player2Choice.equals(Hand.ROCK.choice)) {
                    System.out.println("Tie");
                } else if (player2Choice.equals(Hand.PAPER.choice)) {
                    ++player2Wins;
                    ++rounds;
                    System.out.println("Player2 wins " + player2Wins);

                } else if (player2Choice.equals(Hand.SCISSORS.choice)) {
                    ++player1Wins;
                    ++rounds;
                    System.out.println("Player1 wins " + player1Wins);

                }
            }
            if (player1Choice.equals(Hand.SCISSORS.choice)) {
                System.out.println("Player one played " + player1Choice);
                System.out.println("Player two played " + player2Choice);

                if (player2Choice.equals(Hand.SCISSORS.choice)) {
                    System.out.println("Tie");
                } else if (player2Choice.equals(Hand.ROCK.choice)) {
                    ++player2Wins;
                    ++rounds;
                    System.out.println("Player2 wins " + player2Wins);
                } else if (player2Choice.equals(Hand.PAPER.choice)) {
                    ++player1Wins;
                    ++rounds;
                    System.out.println("Player1 wins " + player1Wins);
                }
            }
            if (player1Choice.equals(Hand.PAPER.choice)) {
                System.out.println("Player one played " + player1Choice);
                System.out.println("Player two played " + player2Choice);

                if(player2Choice.equals(Hand.PAPER.choice)) {
                    System.out.println("Tie");
                } else if (player2Choice.equals(Hand.ROCK.choice)) {
                    ++player1Wins;
                    ++rounds;
                    System.out.println("Player1 wins " + player1Wins);
                } else if (player2Choice.equals(Hand.SCISSORS.choice)) {
                    ++player2Wins;
                    ++rounds;
                    System.out.println("Players2 wins" + player2Wins);
                }
            }
        }
        if(player1Wins > player2Wins){
            System.out.println("The winner is Player 1");
        } else {
            System.out.println("The winner is Player 2");
        }
    }
    }