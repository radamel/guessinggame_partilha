package org.academiadecodigo.bootcamp;

import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {

        Game game = new Game();
        Players player1 = new Players();
        Players player2 = new Players();

            int correctAnswer = game.correctNumberGame();

        System.out.println("I want to play a game!");
        System.out.println("I'm thinking about  ... " + correctAnswer);
        System.out.println("What's your guess ?");


        while (true) {
            int player1Guess = player1.playerGuess();
            int player2Guess = player2.playerGuess();

            System.out.println(player1Guess);
            System.out.println(player2Guess);

            if (correctAnswer == player1Guess) {
                System.out.println("Awesome, Player1! The number is " + correctAnswer);
                break;
            } else if (correctAnswer == player2Guess) {
                System.out.println("Awesome, Player2! The number is " + correctAnswer);
                break;
            } else {
                System.out.println("Try again");
            }

        }
    }
}

